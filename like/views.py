from django.shortcuts import render

from django.views.generic import View
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
import json
from django.db import IntegrityError
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods

from .models import Like

@require_http_methods(["POST"])
@login_required
def like(request, content_type_id=None, pk=None):
    info = request.user.info
    content_type = get_object_or_404(ContentType, id=content_type_id)
    model_class = content_type.model_class()
    object = get_object_or_404(model_class, id=pk)
    current_likes = Like.objects.filter(user=info, item_type=content_type,
                                item_id=pk)
    if (current_likes.exists()):
        current_likes.delete();

        return HttpResponse(json.dumps({"success" : True,
                                        "status": "deleted",
                                        "new": Like.objects.filter(item_type=content_type,
                                                                    item_id=pk).count()}),
                                        content_type="application/json")

    like = Like(user=info, item_type=content_type, item_id=pk)
    try:
        like.save()
    except IntegrityError as e:
        return HttpResponse(json.dumps({"success" : False,
                                        "errors": "dublicate",
                                        "new" : Like.objects.filter(item_type=content_type,
                                                                    item_id=pk).count()}),
                            content_type="application/json")
    return HttpResponse(json.dumps({"success" : True, "status" : "set",
                                    "new" : Like.objects.filter(item_type=content_type,
                                                                item_id=pk).count()}),
                        content_type="application/json")
