from django.db import models

# Create your models here.


from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from user.models import UserInfo

class Like(models.Model):
    user = models.ForeignKey(UserInfo, verbose_name=u'Пользователь')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата лайка')
    item_type = models.ForeignKey(ContentType)
    item_id = models.PositiveIntegerField()
    item = GenericForeignKey('item_type', 'item_id')

    class Meta:
        verbose_name = "Лайк"
        verbose_name_plural = "Лайки"
        unique_together = ["user", "item_type", "item_id"]

    def __str__(self):
        model_class = self.item_type.model_class()
        object = model_class.objects.get(pk=self.item_id)
        return "From %s to %s instance: %s" % (self.user.__str__(), self.item_type.__str__(), object.__str__())


class LikeMixin(models.Model):
    likes = GenericRelation(Like,
                            content_type_field='item_type',
                            object_id_field ='item_id')

    @models.permalink
    def get_like_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        return 'core:like:like', (), {'content_type_id': content_type.id,
                                     'pk': self.id}


    class Meta:
        abstract = True
