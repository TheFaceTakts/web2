from django.conf.urls import url, include
from django.contrib.auth.views import login, logout

from . import views

urlpatterns = [
    url(r'^(?P<content_type_id>\d+)/(?P<pk>\d+)/$', views.like, name='like'),
]
