from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^user/(?P<pk>\d+)/$', views.UserDetail.as_view(), name='user'),
    url(r'^users/$', views.Users.as_view(), name='users'),
]
