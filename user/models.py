from django.db import models
from django.conf import settings
from .templatetags.filename import filename
from django.contrib.contenttypes.models import ContentType
# Create your models here.
import blog
import like
import comment

from PIL import Image
import os

def crop(name):
    path = settings.MEDIA_ROOT + name
    img = Image.open(path)
    half_the_width = img.size[0] / 2
    half_the_height = img.size[1] / 2
    shift = min(half_the_width, half_the_height)
    cropped_img = img.crop(
        (
            half_the_width - shift,
            half_the_height - shift,
            half_the_width + shift,
            half_the_height + shift
        )
    )
    cropped_path = settings.MEDIA_ROOT + "cropped/" + name
    cropped_img.save(cropped_path)
    return cropped_path

class UserInfo(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                related_name='info',
                                verbose_name='Аккаунт')

    userpic = models.ImageField(verbose_name=u'Изображение',
                                default='/media/default-avatar.png')

    class Meta:
        verbose_name = u'Информация о пользователе'
        verbose_name_plural = u'Информация о пользователях'

    @property
    def get_rating(self):
        return like.models.Like.objects.filter(item_type=ContentType.objects.get_for_model(blog.models.Post), item_id__in=self.posts.all()).count() + \
               like.models.Like.objects.filter(item_type=ContentType.objects.get_for_model(comment.models.Comment), item_id__in=self.posts.all()).count()

    def save(self, *args, **kwargs):
        super(UserInfo, self).save(*args, **kwargs)
        crop(filename(self.userpic))
    def __str__(self):
        return self.user.username
