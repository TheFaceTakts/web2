from django.shortcuts import render

from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .models import UserInfo
# Create your views here.


class UserDetail(DetailView):
    model = UserInfo
    template_name = "user/user.html"


class Users(ListView):
    model = UserInfo
    template_name = "user/all_users.html"
