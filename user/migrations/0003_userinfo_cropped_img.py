# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-06-21 23:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0002_userinfo_userpic'),
    ]

    operations = [
        migrations.AddField(
            model_name='userinfo',
            name='cropped_img',
            field=models.ImageField(default='/media/', upload_to='', verbose_name='Квадратное изображение'),
        ),
    ]
