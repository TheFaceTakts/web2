# coding: utf-8
from django import forms
from django.forms import ModelForm
from .models import UserInfo

class UserInfoForm(ModelForm):
    class Meta:
        model = UserInfo
        fields = ['userpic']
