$(document).ready(function() {
    function replaceTextarea(textarea, data, raw) {
        textarea.next().remove(); // <br>
        textarea.next().remove(); // button
        textarea.replaceWith($("<div>").html(data)
                                   .data("edit-url", textarea.data("edit-url"))
                                   .data("raw", raw));
    }
    $(".edit-comment").click(function(event) {
        event.preventDefault();
        var text = $(this).parent().next();
        if (text.is("textarea")) {
            replaceTextarea(text, text.data("old-content"), text.data("old-raw"));
            return;
        }
        var height = text.css("height");
        var edit_url = text.data("edit-url");
        var textarea = $("<textarea>").html(text.data("raw"))
                                      .addClass("form-control")
                                      .css("height", height)
                                      .css("height", "+=20px")
                                      .data("old-content", text.html())
                                      .data("edit-url", edit_url)
                                      .data("old-raw", text.data("raw"));
        text.replaceWith(textarea);
        var submitButton = $("<button>").addClass("btn btn-primary btn-block").text("Редактировать комментарий");
        submitButton.click(function() {
            var info = $("#ajax-data")
            $.ajax({
                url : edit_url,
                type : "POST",
                data : { "new_value": textarea.val(),
                         "csrfmiddlewaretoken": info.data("csrf")}, // data sent with the post request
                // handle a successful response
                success : function(json) {
                    if (json["success"]) {
                        console.log(json);
                        replaceTextarea(textarea, json["new-data"], json["new-raw"]);
                    } else {
                        console.log(json);
                        $("#error-modal-error-text").html(json["errors"]);
                        $("#error-modal").modal();
                    }
                },

                // handle a non-successful response
                error : function(xhr, errmsg, err) {
                    $("#error-modal-error-text").html(errmsg + "\n" + xhr.status + ": " + xhr.responseText);
                    $("#error-modal").modal();
                }
            });
        });
        textarea.after(submitButton).after($("<br>"));
    });
});
