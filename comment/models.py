from django.db import models
import blog
import user
from like.models import LikeMixin
# Create your models here.


class Comment(LikeMixin):
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=u'Время создания')
    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name=u'Время изменения')
    post = models.ForeignKey(blog.models.Post, related_name='comments',
                             verbose_name=u'Пост')

    author = models.ForeignKey(user.models.UserInfo,
                               related_name='comments',
                               verbose_name=u'Автор')

    class Meta:
        verbose_name = u'Комментарий'
        verbose_name_plural = u'Комментарии'

    def __str__(self):
        return self.text[:50]
