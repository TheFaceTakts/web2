from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^edit/(?P<pk>\d+)/$', views.comment_update, name='edit'),
]
