from django.http import HttpResponse
from . import models
from blog import forms
from blog.templatetags.markdown_filter import markdownify
import json


def comment_update(request, pk=None, next_page="/"):
    if request.method == 'POST':
        currentComment = models.Comment.objects.get(pk=pk)
        if (not currentComment):
            return HttpResponse(json.dumps({"success" : False,
                                            'errors': "no such comment"}),
                                content_type = "application/json")
        if (request.user != currentComment.author.user):

            return HttpResponse(json.dumps({"success" : False,
                                            'errors': "You are not the author of the comment"}),
                                content_type = "application/json")
        if (not request.POST.get("new_value")):
            return HttpResponse(json.dumps({"success" : False,
                                            'errors': "No text was provided"}),
                                content_type = "application/json")

        currentComment.text = request.POST.get("new_value")
        currentComment.save()
        return HttpResponse(json.dumps({"success" : True,
                                        "new-raw" : currentComment.text,
                                        "new-data" : markdownify(currentComment.text)}),
                                        content_type = "application/json")
    return HttpResponse(json.dumps({"success" : False,
                                    'errors': "wrong method"}),
                        content_type = "application/json")
