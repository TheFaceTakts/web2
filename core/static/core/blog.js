$(document).ready(function() {
    $('input[type=file]').bootstrapFileInput();
    $('.file-inputs').bootstrapFileInput();
});

hljs.initHighlightingOnLoad();

$("#" + $("#menu").data("active")).addClass("active");

$(document).ready(function() {
    $(document).on("click", ".login-button", function() {
        $("#myModal").modal();
    });
});


$(document).ready(function() {
    $("#login_form").submit(function(event) {
        $("#incorrect-credentials").fadeOut();
        event.preventDefault();
        var info = $("#ajax-data")
        $.ajax({
            url : info.data("url"),
            type : "POST",
            data : { "username": $("#username").val(),
                     "password": $("#psw").val(),
                     "csrfmiddlewaretoken": info.data("csrf")}, // data sent with the post request
            // handle a successful response
            success : function(json) {
                if (json["success"]) {
                    $('#myModal').modal('toggle');
                    location.reload();
                } else {
                    $("#incorrect-credentials").fadeIn();
                }
            },

            // handle a non-successful response
            error : function(xhr, errmsg, err) {
                $("#error-modal-error-text").html(errmsg + "\n" + xhr.status + ": " + xhr.responseText);
                $("#error-modal").modal();
            }
        });
    });

});
