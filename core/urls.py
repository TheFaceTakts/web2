from django.conf.urls import url, include
from django.contrib.auth.views import login, logout

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^comment/', include('comment.urls', namespace='comment')),
    url(r'^user/', include('user.urls', namespace='user')),
    url(r'^logout/(?P<next_page>.*)/$', logout, name='logout'),
    url(r'^login/$', views.login_user, name='login'),
    url(r'^register/$', views.register, name='register'),
    url(r'^register/(?P<next_page>.*)/$', views.register, name='register'),
    url(r'^like/', include('like.urls', namespace='like')),
]
