from django.core.management import BaseCommand
from io import BytesIO
import gzip
from urllib.request import Request, urlopen

from user.models import UserInfo
from blog.models import Post
from django.contrib.auth.models import User
from application import folders

import random
import json
import uuid


from html.parser import HTMLParser
import html2text

def handle_new_post(name, image_url, article_link):
    name = "".join(name.split())
    print(name)
    print("https://blog.ghost.org" + article_link)
    req = Request("https://blog.ghost.org" + article_link,
                  headers={"User-Agent" : "Magic Browser"})
    con = urlopen(req)
    page = con.read().decode("utf-8")
    title = page.split('<h1 class="post-title">')[1].split("</h1>")[0]
    post = page.split('<section class="post-content">')[1].split("</section>")[0]

    post = html2text.html2text(post)
    post = post.replace("(/content/images", "(https://blog.ghost.org/content/images")
    if (not User.objects.filter(username=name).exists()):
        user = User.objects.create_user(name, '', "qwerty123")
        image = urlopen("https:" + image_url).read()
        image_path = folders.MEDIA_ROOT + str(uuid.uuid4())[:20] + ".jpeg"
        image_file = open(image_path, "wb")
        image_file.write(image)
        image_file.close()
        UserInfo(user=user, userpic=image_path).save()

    user = User.objects.filter(username=name)[0]
    Post(title=title, text=post, author=user.info).save()


class MyHTMLParser(HTMLParser):
    def __init__(self):
        self.author_started = None
        self.authore_image = None
        self.author_data = None
        return super(MyHTMLParser, self).__init__()

    def handle_starttag(self, tag, attrs):
        #print("Start tag:", tag)
        if (('alt', 'Author image') in attrs):
            self.authore_image = dict(attrs)['src']
            print("1", self.authore_image)
        if (('class', 'author-name') in attrs):
            self.author_started = 1
        if (self.author_started == 1 and str(tag) == "a"):
            self.author_started = 2
        if (('class', 'post-excerpt') in attrs):
            self.author_started = 4
        if (self.author_started == 4 and str(tag) == "a"):
            self.link = dict(attrs)['href']
            print(self.link)
            handle_new_post(self.author_data, self.authore_image, self.link)
            self.author_started = None

    def handle_endtag(self, tag):
        pass
        #print("Encountered an end tag :", tag)

    def handle_data(self, data):
        if (self.author_started == 2):
            self.author_started = 3
            self.author_data = data
            print("2", self.author_data)
        #print("Encountered some data  :", data)

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        req = Request("https://blog.ghost.org/page/3", headers={"User-Agent" : "Magic Browser"})
        con = urlopen(req)
        page = con.read().decode("utf-8")
        parser = MyHTMLParser()
        print(parser.feed(page))
