from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
import json
from user.models import UserInfo
from user.forms import UserInfoForm
from PIL import Image
from io import StringIO
import base64

def image_to_string(image):
    pic = StringIO()
    return StringIO(base64.b64decode(image))

def verify_image(image):
    image = Image.open(image_to_string(image))
    return image.verify()

#http://www.djangobook.com/en/2.0/chapter14.html
def register(request, next_page="/"):
    if request.method == 'POST':
        print(request.POST)
        form = UserCreationForm(request.POST)
        if form.is_valid():
            image = None #request.POST.get("userpic")
            print(image)

            if (image is None or verify_image(image)):
                new_user = form.save()
                user = authenticate(username=form.cleaned_data['username'],
                                        password=form.cleaned_data['password1'],
                                        )
                login(request, user)
                if (image):
                    print(image, image_to_string(image))
                    info = UserInfo(user=user, userpic=image_to_string(image))
                    info.save()
                else:
                    info = UserInfo(user=user)
                    info.save()


                    return HttpResponse(json.dumps({"success" : True, "next" : next_page}), content_type = "application/json")
            #return HttpResponse(json.dumps({"success" : False, 'errors': dict(user_info_form.errors.items())}), content_type = "application/json")
        return HttpResponse(json.dumps({"success" : False, 'errors': dict(form.errors.items())}), content_type = "application/json")

    form = UserCreationForm()
    return render(request, "core/register.html", {
        'form': form,
        'next_page': next_page,
    })

def login_user(request):
    username = password = None
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponse(json.dumps({"success" : True}),
                                    content_type = "application/json")
    return HttpResponse(json.dumps({"success" : False}),
                        content_type = "application/json")



def index(request):
    return redirect('core:blog:all_posts')
