# coding: utf-8
from django import forms
from comment.models import Comment
from .models import Post
from django.forms import ModelForm

class NewCommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['text']

class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'text']
