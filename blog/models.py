from django.db import models
from user.models import UserInfo
from like.models import LikeMixin
# Create your models here.

class Post(LikeMixin):
    title = models.CharField(max_length=255)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=u'Время создания')
    updated_at = models.DateTimeField(auto_now=True,
                                      verbose_name=u'Время изменения')
    author = models.ForeignKey(UserInfo, verbose_name=u'Автор',
                               related_name='posts')

    class Meta:
        verbose_name = u'Пост'
        verbose_name_plural = u'Посты'

    def __str__(self):
        return self.title
