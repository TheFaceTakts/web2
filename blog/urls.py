from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^$', views.PostList.as_view(), name='all_posts'),
    url(r'^post/(?P<pk>\d+)/$', views.PostDetail.as_view(), name='post'),
    url(r'^edit/(?P<pk>\d+)/$', views.PostUpdate.as_view(), name='edit'),
    url(r'^new/$', views.PostCreate.as_view(), name='new'),
]
