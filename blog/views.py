from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseForbidden
from django.views.generic import ListView, CreateView, FormView, UpdateView
from . import models
import comment.models
from . import forms
from django.core.urlresolvers import reverse

# Create your views here.


class PostList(ListView):
    model = models.Post
    template_name = "blog/all_posts.html"

class PostDetail(CreateView):
    form_class = forms.NewCommentForm
    model = comment.models.Comment
    template_name = "blog/post.html"

    def get_success_url(self):
        return self.request.path

    def form_valid(self, form):
        form.instance.author = self.request.user.info
        form.instance.post = self.post_related
        return super(PostDetail, self).form_valid(form)

    def dispatch(self, request, pk=None, *args, **kwargs):
        self.user = request.user
        self.post_related = get_object_or_404(models.Post, id=pk)
        return super(PostDetail, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PostDetail, self).get_context_data(**kwargs)
        context['post'] = self.post_related
        context['comments'] = self.post_related.comments.all()
        return context

class PostUpdate(UpdateView):
    model = models.Post
    form_class = forms.PostForm
    template_name = "blog/edit_post.html"

    def get_success_url(self):
        return reverse('core:blog:post', args=[self.pk])

    def dispatch(self, request, pk=None, *args, **kwargs):
        self.pk = pk
        if self.get_object().author.user != self.request.user:
            return HttpResponseForbidden()
        return super(PostUpdate, self).dispatch(request, *args, **kwargs)

class PostCreate(CreateView):
    model = models.Post
    form_class = forms.PostForm
    template_name = "blog/edit_post.html"

    def get_success_url(self):
        return reverse('core:blog:post', args=[self.object.pk])

    def form_valid(self, form):
        form.instance.author = self.request.user.info
        return super(PostCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PostCreate, self).get_context_data(**kwargs)
        context['title'] = "Создать пост"
        return context
